def getLines(filename):
    lines = []
    f = open(filename)
    line = f.readline()
    count = 0
    while line:
        count = count + 1
        lines.append(line)
        line = f.readline()
    return lines, count

rawData, initialCount = getLines('data/data.csv')
missingText = "Missing"
processedData = []
for line in rawData:
    cols = line.split(',')
    for i in range(len(cols)):
        cols[i] = cols[i].strip()
        if cols[i] == '':
            cols[i] = missingText
    processedData.append(cols)

def getDiscrete(rvFeatures,featureRange,data):
    rvStrat = [[] for x in featureRange]
    for i in rvFeatures:
        vals = []
        for j in range(len(data)):
            if data[j][i] != missingText:
                vals.append(float(data[j][i]))
        vals = list(set(vals))
        vals.sort()
        strat = [(x+1)*len(vals)/5 for x in range(4)]
        strats = []
        for s in strat:
            strats.append(vals[s])
        rvStrat[i] = strats
    rvVec = []
    for j in range(len(data)):
        vals = []
        for i in rvFeatures:
            if data[j][i] == missingText:
                label = missingText
            else:
                label = '>={0}'.format(rvStrat[i][3])
                val = float(data[j][i])
                for s in rvStrat[i]:
                    if val<s:
                        label = '<{0}'.format(s)
                        break
            vals.append(label)
        rvVec.append(vals)
    return rvVec

def getBit(bvFeatures,featureRange,data):
    bvValues = [[] for x in featureRange]
    for i in bvFeatures:
        vals = []
        for j in range(len(data)):
            vals.append(data[j][i].strip())
        bvValues[i] = list(set(vals))
    bvVec = []
    for j in range(len(data)):
        vals = []
        for i in bvFeatures:
            vec = [0 for x in range(len(bvValues[i]))]
            vec[bvValues[i].index(data[j][i].strip())] = 1
            vals.extend(vec)
        bvVec.append(vals)
    return bvVec

featureRange = range(62)
rvFeatures = [0,2,6,13,14,15,16,17,18,25,26,28,31]
catFeatures = [1,3,4,5,7,8]
rvMissingFeatures = [9,10,11,12,19,20,21,22,23,24,27,29,30,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,61]

catBit = getBit(catFeatures,featureRange,processedData)
missingDiscrete = getDiscrete(rvMissingFeatures,featureRange,processedData)
missingBit = getBit(range(len(missingDiscrete[0])),range(len(missingDiscrete[0])),missingDiscrete)

finalData = []
for j in range(len(processedData)):
    vals = []
    for i in rvFeatures:
        vals.append(float(processedData[j][i]))
    vals.extend(catBit[j])
    vals.extend(missingBit[j])
    finalData.append(vals)

def writeData(filename,data):
    with open(filename,'w') as f:
        for line in data:
            f.write(', '.join(str(x) for x in line))
            f.write('\n')

writeData('data/processedData.data',finalData)
